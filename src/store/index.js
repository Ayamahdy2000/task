import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
import club from "../data/club.js"
export default new Vuex.Store({
  state: {
    club
  },
  actions: {
      addClub: ({ commit }, Clubs) => {
        commit('appendClub', Clubs)
      }
    
  },
  mutations: {
    appendClub: (state, Clubs) => {
      state.club.push(Clubs)
    }
  },
  
  modules: {},
});